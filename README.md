denkmal.org 2004-2009
=====================

Historical repo of denkmal.org.
- **Functionality**: List of parties, list of pizza delivery services, weather forecast, WAP and later iPhone version.
- **Technology**: Perl.


---
June 2004:

![Screenshot 2004-06-16](docs/screenshot-2004-06-16.png)

---
August 2004:

![Screenshot 2004-08-09](docs/screenshot-2004-08-09.png)

---
"PizzaPizza" page 2005:

![Screenshot 2005-02-04](docs/screenshot-2005-02-04-pizzapizza.png)

---
2007:

![Screenshot 2005-02-04](docs/screenshot-2007-05-15.png)
