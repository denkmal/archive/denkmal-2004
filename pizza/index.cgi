#!/usr/bin/perl
use DBI;
use CGI;
use Time::Local;
use CGI::Carp qw(fatalsToBrowser);

$query = CGI::new();
my $q = $query->param("q");
my $morninghour=7;
my $htmlfile="denkmal.org.html";
my ($Sekunden,$Minuten,$Stunden,$Monatstag,$Monat,$Jahr,$Wochentag) = localtime(time);
if ($Stunden<$morninghour) {
 ($tmp,$tmp,$tmp,$Monatstag,$Monat,$Jahr,$Wochentag) = localtime(time-86400); 
}
$Monat++; $Jahr+=1900; $Wochentag--;
my $currdatumsql = "$Stunden:$Minuten:$Sekunden";
my $heute=$Wochentag;
my @tage = ("mo","di","mi","do","fr","sa","so"); my $heutesql = $tage[$heute];
my @open;
my @closed;
my $dots;
my $list;
my $news;
my $imgload;
my $info="PIZZAKURIERE IN BASEL FÜR HEUTE " .uc($heutesql). ", $Monatstag.$Monat. ";

my %bilder = (
	"Pizza Bella" => "Pizza_Bella_salame.gif",
	"Pizza Express Cedro" => "Pizza_Express_Cedro_salame.gif",
	"Pizza Express Spalenberg" => "Pizza_Express_Spalenberg_salame.gif",
	"XXLARGE Pizzakurier" => "XXLARGE_Pizzakurier_salame.gif",
	"Pizza Teresa" => "Pizza_Teresa_salame.gif",
	"Pizza Veloce" => "Pizza_Veloce_salame.gif",
	"Best Pizza" => "Best_Pizza_salame.gif",
	"Pizza Quick" => "Pizza_Quick_salame.gif",
	"Ali Baba Kurier" => "Ali_Baba_Kurier_salame.gif",
	"Fantasia" => "Fantasia_salame.gif",
	"Pizza Melsa" => "Pizza_Melsa_salame.gif",
	"Pizza Blitz" => "Pizza_Blitz_prosciutto.gif",
	"Pizza Phone" => "Pizza_Phone_salame.gif"
);

if ($Stunden<$morninghour) { $info.="NACHTS"; }
else { $info.="$Stunden:$Minuten UHR"; }

my $dbh = DBI->connect("DBI:mysql:withinc_denkmal:localhost","withinc_denkmal","thedog") or die "DB error\n";

my $i=0;
foreach $key (keys(%bilder)) {
 $imgload.="pizza$i = new Image(); pizza$i.src = \"/pics/pizzas/" .$bilder{$key}. "\";\n";
 $i++;
}

$Select = $dbh->prepare("SELECT * FROM pizzakurier order by margherita");
$Select->execute();

while($Row=$Select->fetchrow_hashref) {
 $open1=$Row->{$heutesql.'1'}; $open1_=$Row->{$heutesql.'2'};
 $open2=$Row->{$heutesql.'3'}; $open2_=$Row->{$heutesql.'4'};
 if ($open1=~/(\d+):(\d+):(\d+)/) {$oh[0]=$1;$om[0]=$2;$os[0]=$3;}
 if ($open1_=~/(\d+):(\d+):(\d+)/) {$oh_[0]=$1;$om_[0]=$2;$os_[0]=$3;}
 if ($open2=~/(\d+):(\d+):(\d+)/) {$oh[1]=$1;$om[1]=$2;$os[1]=$3;}
 if ($open2_=~/(\d+):(\d+):(\d+)/) {$oh_[1]=$1;$om_[1]=$2;$os_[1]=$3;}

 my $status=0;
 my $i=0;
 foreach(@oh) {
  if ($oh[$i] != 0 || $om[$i] != 0 || $os[$i] != 0) {
   if ($oh[$i]==24) { $oh[$i]=23; $om[$i]=59; $os[$i]=59; }
   if ($oh_[$i]==24) { $oh_[$i]=23; $om_[$i]=59; $os_[$i]=59; }
   $currsecs = timelocal($Sekunden,$Minuten,$Stunden,$Monatstag,$Monat-1,$Jahr);
   if ($Stunden<$morninghour && $oh_[$i]<$morninghour) {
    if ($currsecs<=timelocal($os_[$i],$om_[$i],$oh_[$i],$Monatstag,$Monat-1,$Jahr)) { $status=1; }
   } elsif ($oh_[$i]<$morninghour) {
    if ($currsecs>=timelocal($os[$i],$om[$i],$oh[$i],$Monatstag,$Monat-1,$Jahr)) { $status=1; }
   } else {
    if ($currsecs>=timelocal($os[$i],$om[$i],$oh[$i],$Monatstag,$Monat-1,$Jahr) && $currsecs<=timelocal($os_[$i],$om_[$i],$oh_[$i],$Monatstag,$Monat-1,$Jahr)) { $status=1; }
   }
  }
  $i++;
 } 

 if ($status == 1) { push(@open,$Row); }
 else { push(@closed,$Row); }

 $dot="<div class=\"dot\" style=\"$Row->{coords}\">";
 $dot.="<a target=\"_blank\" href=\"$Row->{url}\" " .javascriptmouse($Row,1). ">";
 if ($status==1) { $dot.="<img src=\"/pics/dot1.gif\" border=\"0\" id=\"dot_$Row->{name}\">"; }
 else { $dot.="<img src=\"/pics/dot2.gif\" border=\"0\" id=\"dot_$Row->{name}\">"; }
 $dot.="</a></div>";
 $dots.=$dot; 
}

$Select->finish();


$list.="<table style=\"border-collapse:collapse;border:0;padding:0;margin-left:22px;\">";

foreach $Row (@open) {
 $list.="<tr style=\"cursor:default;\"" .javascriptmouse($Row,0). ">";
 $list.="<td style=\"padding-right:10px\"><img src=\"/pics/dot1.gif\"> ";
 $list.="<a href=\"$Row->{url}\" class=\"list\" title=\"$Row->{url}\">$Row->{name}</a>";
 if ($Row->{onlineshop} ne '') { $list.=" (<a href=\"$Row->{onlineshop}\" class=\"list\">Onlineshop</a>)"; }
 $list.="</td><td style=\"padding-right:10px\">";
 $list.="<i>" .telefon($Row->{telefonnummer}). "</i>  ";
 $list.="</td><td style=\"padding-right:10px\">";
 if($Row->{margherita}>0) { $list.="Margherita: " .geld($Row->{margherita}); }
 $list.="</td><td>";
 if($Row->{mindestbetrag}!=0) { $list.="(Min: " .geld($Row->{mindestbetrag}). ")"; }
 $list.="</td></tr>";
}

foreach $Row (@closed) {
 $list.="<tr style=\"cursor:default;\"" .javascriptmouse($Row,0). ">";
 $list.="<td style=\"padding-right:10px\"><img src=\"/pics/dot2.gif\"> ";
 $list.="<a href=\"$Row->{url}\" class=\"list\" title=\"$Row->{url}\">$Row->{name}</a>";
 if ($Row->{onlineshop} ne '') { $list.=" (<a href=\"$Row->{onlineshop}\" class=\"list\">Onlineshop</a>)"; }
 $list.="</td><td style=\"padding-right:10px\">";
 $list.="<i>" .telefon($Row->{telefonnummer}). "</i>  ";
 $list.="</td><td style=\"padding-right:10px\">";
 if($Row->{margherita}>0) { $list.="Margherita: " .geld($Row->{margherita}); }
 $list.="</td><td>";
 if($Row->{mindestbetrag}!=0) { $list.="(Min: " .geld($Row->{mindestbetrag}). ")"; }
 $list.="</td></tr>";
}

$list.="</table>"; 

open(HTML,$htmlfile);
@html=<HTML>;
close(HTML);

$i=0;
foreach(@html) {
 $html[$i]=~s/\[DOTS\]/$dots/g;
 $html[$i]=~s/\[LIST\]/$list/g;
 $html[$i]=~s/\[OTHER\]//g;
 $html[$i]=~s/\[NEWS\]/$news/g;
 $html[$i]=~s/\[INFO\]/$info/g;
 $html[$i]=~s/\[IMGLOAD\]/$imgload/g;
 $i++;
}

print "Content-type: text/html\n\n";
print @html;







sub javascriptmouse() {
 local($Row,$box) = @_;
 local $str;
 $str = "onMouseOver=\"mousein('$Row->{name}','<a href=\\'$Row->{url}\\'><b>$Row->{name}</b></a>";
 if ($Row->{onlineshop} ne '') { $str.=" (<a href=\\'$Row->{onlineshop}\\'>Onlineshop</a>)"; }
 $str.="<br>Telefon: " .telefon($Row->{telefonnummer}). "<br>";
 if ($Row->{mindestbetrag}!=0) { $str.="Mindestbestellwert: " .geld($Row->{mindestbetrag}). "<br>"; }
 $str.="Margherita: " .geld($Row->{margherita}). " / ";
 $str.="Prosciutto: " .geld($Row->{prosciutto}). " / ";
 $str.="Salame: " .geld($Row->{salame}). "<br><hr>";
 $str.="<table style=\\'border-collapse:collapse;padding:0;border:0;\\' width=\\'100%\\'><tr><td>";
 foreach(@tage) {
  if ($Row->{$_.'1'} ne "00:00:00") { 
   if ($heutesql eq $_) { $str.="<b>"; }
   $str.=ucfirst($_). ": " .zeit($Row->{$_.'1'}). "-" .zeit($Row->{$_.'2'});
  }
  if ($Row->{$_.'3'} ne "00:00:00") { $str.=" /  " .zeit($Row->{$_.'3'}). "-" .zeit($Row->{$_.'4'}); }
  if ($Row->{$_.'1'} ne "00:00:00") { 
   if ($heutesql eq $_) { $str.="</b>"; }
   $str.="<br>";
  }
 }
 undef $imgstr;
 if ($bilder{$Row->{name}}) { $imgstr="<img src=\\'/pics/pizzas/".$bilder{$Row->{name}}."\\'>"; }
 $str.="</td><td align=\\'right\\' valign=\\'top\\'>$imgstr</td></tr></table>";
 $str.="','$box');\" onMouseOut=\"mouseout('$Row->{name}');\"";
 return $str;
}


sub telefon() {
 local($str)=@_;
 if ($str=~/^\+41(\d+)/) { $str=$1; }
 if ($str=~/^(8\d{2})(\d+)/) { $str="0$1 $2"; }
 elsif ($str=~/^(\d{2})(\d+)/) { $str="0$1 $2"; } 
 return $str;
}

sub geld() {
 local($str)=@_;
 if ($str=~/(\d+)\.(\d+)/) { $tmp1=$1; $tmp2=$2; 
  if ($tmp2=~/^\d$/) { $tmp2=$tmp2.'0'; }
  if ($tmp2=~/^(\d\d)\d+$/) { $tmp2=$1; }
  if($tmp1<0 && $tmp2<=0) { return "0.-"; }
  if($tmp1!=0 || $tmp2!=0) { return "$tmp1,$tmp2.-"; }
 } else {
  if ($str<0) { return "0.-"; }
  if ($str!=0) { return "$str.-"; }
 }
}

sub zeit() {
 local($str)=@_;
 if ($str =~ /(\d{1,2}):(\d{1,2}):(\d{1,2})/) {
  $h=$1;$m=$2;
  if ($m=~/^\d$/) { $m="0$m"; }
  return "$h:$m";
 } else {
  return $str;
 }
}
