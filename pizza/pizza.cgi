#!/usr/bin/perl
use DBI;
use CGI;
use Time::Local;
use CGI::Carp qw(fatalsToBrowser);

$query = CGI::new();
my $q = $query->param("q");
my $morninghour=7;
my ($Sekunden,$Minuten,$Stunden,$Monatstag,$Monat,$Jahr,$Wochentag) = localtime(time); $Monat+=1; $jahr+=1900; $Wochentag--;
my $currdatumsql = "$Stunden:$Minuten:$Sekunden";
my $heute=$Wochentag; if ($Stunden<$morninghour) { $heute--; if ($heute<0) { $heute=6; } }
my @tage = ("mo","di","mi","do","fr","sa","so"); my $heutesql = $tage[$heute];
my @open;
my @closed;

my $dbh = DBI->connect("DBI:mysql:withinc_denkmal:localhost","withinc_denkmal","thedog") or die "DB error\n";

print "Content-type: text/html\n\n";
print '<html><head><title>Pizzalieferdienst Basel</title><style>';
print 'table,td,tr { border: 2px gray solid; padding:5px; border-collapse:collapse; }';
print '.fett { color:white; font-weight:600; }';
print '</style></head><body>';
print "<b>Pizzalieferdienst Basel: " .ucfirst($heutesql). ", $Monatstag.$Monat. " .zeit("$Stunden:$Minuten:$Sekunden"). " Uhr</b><br><br>";

print '<table><tr><td bgcolor="#D0FFBE">open</td><td bgcolor="#FFBEBE">closed</td></tr></table>Korrekturen an: <a href="mailto:pizza&#64;retoonline.com">pizza&#64;retoonline.com</a><br><br>';

$Select = $dbh->prepare("SELECT * FROM pizzakurier order by margherita");
$Select->execute();

while($Row=$Select->fetchrow_hashref) {
 $open1=$Row->{$heutesql.'1'}; $open1_=$Row->{$heutesql.'2'};
 $open2=$Row->{$heutesql.'3'}; $open2_=$Row->{$heutesql.'4'};
 if ($open1=~/(\d+):(\d+):(\d+)/) {$oh[0]=$1;$om[0]=$2;$os[0]=$3;}
 if ($open1_=~/(\d+):(\d+):(\d+)/) {$oh_[0]=$1;$om_[0]=$2;$os_[0]=$3;}
 if ($open2=~/(\d+):(\d+):(\d+)/) {$oh[1]=$1;$om[1]=$2;$os[1]=$3;}
 if ($open2_=~/(\d+):(\d+):(\d+)/) {$oh_[1]=$1;$om_[1]=$2;$os_[1]=$3;}

 my $status=0;
 my $i=0;
 foreach(@oh) {
  if ($oh[$i] != 0 || $om[$i] != 0 || $os[$i] != 0) {
   if ($oh[$i]==24) { $oh[$i]=23; $om[$i]=59; $os[$i]=59; }
   if ($oh_[$i]==24) { $oh_[$i]=23; $om_[$i]=59; $os_[$i]=59; }
   $currsecs = timelocal($Sekunden,$Minuten,$Stunden,$Monatstag,$Monat-1,$Jahr);
   if ($Stunden<$morninghour && $oh_[$i]<$morninghour) {
    if ($currsecs<=timelocal($os_[$i],$om_[$i],$oh_[$i],$Monatstag,$Monat-1,$Jahr)) { $status=1; }
   } elsif ($oh_[$i]<$morninghour) {
    if ($currsecs>=timelocal($os[$i],$om[$i],$oh[$i],$Monatstag,$Monat-1,$Jahr)) { $status=1; }
   } else {
    if ($currsecs>=timelocal($os[$i],$om[$i],$oh[$i],$Monatstag,$Monat-1,$Jahr) && $currsecs<=timelocal($os_[$i],$om_[$i],$oh_[$i],$Monatstag,$Monat-1,$Jahr)) { $status=1; }
   }
  }
  $i++;
 } 

 if ($status == 1) { push(@open,$Row); }
 else { push(@closed,$Row); }
}

$Select->finish();



print '<table border="1">';
print '<tr height="50" bgcolor="green"><td></td><td><span class="fett">Telefon</span></td><td><span class="fett">Min.Bestellung</span></td><td><span class="fett">Margherita</span></td><td><span class="fett">Prosciutto</span></td><td><span class="fett">Salame</span></td><td><span class="fett">Online Bestellung</span></td></tr>';


foreach $Row (@open) {
 print '<tr bgcolor="#D0FFBE">';
 print "<td><a href=\"$Row->{url}\">$Row->{name}</a></td>";
 print "<td>" .telefon($Row->{telefonnummer}). "</td>";
 print "<td>Min: " .geld($Row->{mindestbetrag}). "</td>";
 print "<td>" .geld($Row->{margherita}). "</td>";
 print "<td>" .geld($Row->{prosciutto}). "</td>";
 print "<td>" .geld($Row->{salame}). "</td>";
 print "<td>"; if($Row->{onlineshop} ne '') { print "<a href=\"$Row->{onlineshop}\">Onlineshop</a>"; } print "</td>";
 print '</tr>';
}

foreach $Row (@closed) {
 print '<tr bgcolor="#FFBEBE">';
 print "<td><a href=\"$Row->{url}\">$Row->{name}</a></td>";
 print "<td>" .telefon($Row->{telefonnummer}). "</td>";
 print "<td>Min: " .geld($Row->{mindestbetrag}). "</td>";
 print "<td>" .geld($Row->{margherita}). "</td>";
 print "<td>" .geld($Row->{prosciutto}). "</td>";
 print "<td>" .geld($Row->{salame}). "</td>";
 print "<td>"; if($Row->{onlineshop} ne '') { print "<a href=\"$Row->{onlineshop}\">Onlineshop</a>"; } print "</td>";
 print '</tr>';
}

 
print '</table>';
print '</body></html>';





sub telefon() {
 local($str)=@_;
 if ($str=~/^\+41(\d+)/) { $str=$1; }
 if ($str=~/^(8\d{2})(\d+)/) { $str="0$1 $2"; }
 elsif ($str=~/^(\d{2})(\d+)/) { $str="0$1 $2"; } 
 return $str;
}

sub geld() {
 local($str)=@_;
 if ($str=~/(\d+)\.(\d+)/) { $tmp1=$1; $tmp2=$2; 
  if ($tmp2=~/^\d$/) { $tmp2=$tmp2.'0'; }
  if ($tmp2=~/^(\d\d)\d+$/) { $tmp2=$1; }
  if($tmp1<0 && $tmp2<=0) { return "0.-"; }
  if($tmp1!=0 || $tmp2!=0) { return "$tmp1,$tmp2.-"; }
 } else {
  if ($str<0) { return "0.-"; }
  if ($str!=0) { return "$str.-"; }
 }
}

sub zeit() {
 local($str)=@_;
 if ($str =~ /(\d{1,2}):(\d{1,2}):(\d{1,2})/) {
  $h=$1;$m=$2;
  if ($m=~/^\d$/) { $m="0$m"; }
  return "$h:$m";
 } else {
  return $str;
 }
}
