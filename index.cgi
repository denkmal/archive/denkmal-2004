#!/usr/bin/perl

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use CGI;
use LWP::UserAgent;
use Encode;
use POSIX;


# Wann beginnt der neue Tag:
my $morninghour = 6;
# HTML-Template:
my $htmlfile = "denkmal.org.html";
my $htmlfile_mobile = "denkmal.org_mobile.html";
my $htmlsrc;
# Relativer Pfad vom Client zu den Bildern:
my $picsdir = "pics";
# Relativer Pfad vom Client zu den Audio Dateien:
my $audiodir = "wasloift/audio";


$query = CGI::new();
my $taggesucht = $query->param("tag");
my $showall = $query->param("showall");
	if ($showall!=1) { $showall=0; }
my $dots, $list, $other, $news;
my ($Sekunden, $Minuten, $Stunden, $Monatstag, $Monat, $Jahr, $Wochentag);


 # MOBILE DEVICE?
 our $mobile=0;		# Showing mobile-enabled version?
 my $mobile_param = $query->param("m");
 my $mobileq;		# URL-Param
 if (isMobile($ENV{HTTP_USER_AGENT})) { $mobile=1; }
 if ($mobile && $mobile_param<0) { $mobileq=-1; } elsif (!$mobile && $mobile_param>0) { $mobileq=1; }
 if ($mobile_param>0) { $mobile=1; } elsif ($mobile_param<0) { $mobile=0; }

 # DATENBANK
 my $dbh = DBI->connect("DBI:mysql:withinc_denkmal:localhost","withinc_denkmal","thedog")  || die "Database error: $DBI::errstr";
 $set = $Select2 = $dbh->prepare("SET NAMES utf8");
 $set->execute; $set->finish;

 # DATUMZEUGS und gesuchter Tag suchen:
 my $datumgesucht, $datumgesucht_sql;
 my @tage = ("So","Mo","Di","Mi","Do","Fr","Sa");
 &getDatum();


 
# CACHING:
my $cache_identifier = "wasloift_" .$datumgesucht_sql. "_" .$showall. "_" .$mobile;
my $cache = getCache($cache_identifier);

if ($cache) {
	$htmlsrc = $cache;

} else {
	# NO CACHE - So do the whole thing:
	 # Get urls to link to:
	 my %NameToUrl;
	 my %NameToOnlyifmarked;
	 getUrls();

	# Denkmal News (side-boxes)
	my $top = 310;
	# Denkmal_SM
	 $Select = $dbh->prepare("SELECT * FROM events WHERE (ort='denkmal_sm' AND id=-100 AND enabled=1)");
	 $Select->execute; 
	 if ($Select->rows > 0) {
		$Row=$Select->fetchrow_hashref;
		if ($Row->{star}) {
			$news .= "<div id='news' class='noprint' style='position:absolute;left:770;top:" .$top. "px;'><img src='/pics/denkmal_sm.jpg'>";
				$news .= "<div id='news_wimpy' class='noprint' style='position:absolute;left:103;top:60;'>" .wimpyCode("$audiodir/".$Row->{audio}, "denkmal_sm", "f8d758"). "</div>";
				$news .= "<div id='news_text' class='noprint' style='position:absolute;left:15;top:85;width:196px;text-align:center;'>" .add_links($Row->{beschreibung}). "</div>";
				$news .= "</div>";
			$top+=152;
			$top+=14;
		}
	}
	# Denkmal_MV
	$news .= "<div id='news2' class='noprint' style='position:absolute;left:770;top:" .$top. "px;'><img src='/pics/denkmal_mv.jpg'>";
		$news .= "<div id='news2_text' class='noprint' style='position:absolute;left:15;top:60;width:196px;text-align:center;'>DENKMAL.ORG to go!<br>Surfe mit deinem Handy nach «www.denkmal.org».</div>";
		$news .= "</div>";
	$top+=261;
	$top+=14;



	 my $info="PARTIES IN BASEL FÜR " .uc($taggesucht). ", $datumgesucht";

	 # Links zu anderen Wochentagen:
	 $n=$Wochentag; undef $str;
	 foreach(@tage) {
	  if ($taggesucht eq $tage[$n] || (!$taggesucht && $Wochentag==$n)) { $str .="<b>"; if($mobile){$str.="<u>";} }
	  my $mobilestr=''; if ($mobileq) { $mobilestr="&m=$mobileq"; }
	  if ($showall==1) { $str .= "<a href=\"?tag=$tage[$n]&showall=1$mobilestr\">$tage[$n]</a>"; }
	  else { $str .= "<a href=\"?tag=$tage[$n]$mobilestr\">$tage[$n]</a>"; }
	  if ($taggesucht eq $tage[$n] || (!$taggesucht && $Wochentag==$n)) { $str .="</b>"; if($mobile){$str.="</u>";} }
	  $str.=" ";
	  $n++; if($n>6) {$n=0;} 
	 }
	 
	 # Add event:
	 $other.= "<div style=\"position:absolute;left:670;top:200; z-index:10\" class='noprint'><a href='/wasloift/add.cgi'><img src='$picsdir/add_event.gif' style='border:0;'></a></div>";

	 if ($mobile) {
		# Andere Tage:
		$other=$str;
	 } else {
		# Andere Tage:
		$other.= "<div class='noprint' style=\"padding:3;-moz-opacity:0.7;filter:alpha(opacity=70);opacity:0.7;background:#56D540;position:absolute;left:350;top:206;width:400;\">$str</div>";
		# Wetter:
		$other.= "<div style=\"padding:3;-moz-opacity:0.7;filter:alpha(opacity=70);opacity:0.7;background:#56D540;position:absolute;left:350;top:240;width:400;\"><img width=\"39\" height=\"36\" src=\"/pics/meteo/meteo01.gif\"><img width=\"39\" height=\"36\" src=\"/pics/meteo/meteo02.gif\"><img width=\"39\" height=\"36\" src=\"/pics/meteo/meteo03.gif\"><img width=\"39\" height=\"36\" src=\"/pics/meteo/meteo04.gif\"></div>";
		# Showall label:
		if ($showall==1) { $other.= "<div class='noprint' style=\"position:absolute;left:3;top:61;\"><a style='font-size:70%;color:#6666AA;' href='?tag=$taggesucht'>Weniger anzeigen</a></div>"; }
		else { $other.= "<div class='noprint' style=\"position:absolute;left:3;top:61;\"><a style='font-size:70%;color:#6666AA;' href='?tag=$taggesucht&showall=1'>Alle anzeigen</a></div>"; }
	 }




	# Events with stars:
	listEvents(1);
	# Events without stars:
	listEvents(0);


	if ($mobile) { $htmlfile = $htmlfile_mobile; }
	open(HTML,$htmlfile);@htmls=<HTML>;
	close(HTML);
	$htmlsrc = join('',@htmls);
	$htmlsrc=~s/\[DOTS\]/$dots/g;
	$htmlsrc=~s/\[LIST\]/<ul>$list<\/ul>/g;
	$htmlsrc=~s/\[OTHER\]/$other/g;
	$htmlsrc=~s/\[NEWS\]/$news/g;
	$htmlsrc=~s/\[INFO\]/$info/g;
	$htmlsrc=~s/\[IMGLOAD\]//g;


	# Set cache:
	setCache($cache_identifier, $htmlsrc);
}

 

# Statistics:
doStats();

$dbh->disconnect();

# Print page:
print "Content-type: text/html\n\n";
print $htmlsrc;




#########################################################################################################


# Get cache-html by identifier:
sub getCache($) {
	my($ident) = @_;
	$Select = $dbh->prepare("SELECT * FROM cache WHERE (name='$ident')");
	$Select->execute; 
	my $cache=$Select->fetchrow_hashref;
	$Select->finish;
	if ($cache) {
		return $cache->{html};
	} else {
		return;
	}
}

# Set cache-html by identifier:
sub setCache($$) {
	my($ident, $html) = @_;
	# Check if we already have a row for this:
	$Select = $dbh->prepare("SELECT * FROM cache WHERE (name='$ident')");
	$Select->execute; 
	my $cache=$Select->fetchrow_hashref;
	$Select->finish;
	if ($cache) {
		$ins = $dbh->prepare("UPDATE cache SET html='" .escapeSql($html). "' WHERE (name = '$ident')");
	} else {
		$ins = $dbh->prepare("INSERT INTO cache (name,html) VALUES('$ident','" .escapeSql($html). "')");
	}
	$ins->execute;
	$ins->finish;
}



# Save user-statistics to db:
sub doStats() {
	# Mobile:
	if ($mobile && !$mobileq) {
		$edit = $dbh->prepare("UPDATE stats SET value=value+1 WHERE name='hits_mobile'");
		$edit->execute; $edit->finish;
	}
}






sub listEvents() {
 my ($hasStar) = @_;
 my $wimpyColor="69d74c"; if ($hasStar) { $wimpyColor="f8d758"; }


 # Alle Locations durchgehen:
 $Select = $dbh->prepare("SELECT * FROM locations WHERE (alias is null) and (enabled = '1')");
 $Select->execute; 
 while ($Row=$Select->fetchrow_hashref) {
	$location_id = $Row->{id};
	$location_name = $Row->{name};
	$location_website = $Row->{website};
	$location_showalways = $Row->{showalways};
	$location_posx = $Row->{posx};
	$location_posy = $Row->{posy};
	
	$location_name_ = $location_name;
	$location_name_=~s/'/\\'/g;
	
  # Is there an event at this location?:
  $Select2 = $dbh->prepare("SELECT * FROM events WHERE (ort = '$location_name_') and (datum = '$datumgesucht_sql') and (enabled = '1') and (blocked = '0')");
  $Select2->execute; 
  $Row2=$Select2->fetchrow_hashref;
  $Select2->finish();
  $event_zeit = $Row2->{zeit};
  $event_zeit2 = $Row2->{zeit2};
  $event_beschreibung = $Row2->{beschreibung};
  $event_star = $Row2->{star};
  
  if ($location_website) { $web="<a href=\\'$location_website\\' target=\\'_blank\\'>$location_website</a>"; } else { $web=""; }
  
  # Location hat heute einen Event:
  if ($Row2->{enabled}) {
   if ($event_star == $hasStar) {
	   my $dot="dot1.gif"; my $dotsize=7;
			if ($event_star==1) { $dot="dot3.gif"; $dotsize=9; }
	   my $zindex=50; if ($event_star==1) { $zindex=100; }
	   if (!$mobile) { $event_beschreibung=add_links($event_beschreibung); }
	   $event_beschreibung_ =  $event_beschreibung;
	   $event_beschreibung_ =~ s/'/\\'/g;
	   if ($event_zeit) { $text = "<b>$location_name_ <img src=\\'$picsdir/$dot\\'></b><br>" .zeitformat($event_zeit,$event_zeit2). " Uhr<br>$event_beschreibung_<br>$web"; } 
	   else { $text = "<b>$location_name_ <img src=\\'$picsdir/$dot\\'></b><br>$event_beschreibung_<br>$web"; }
	   $dots .= "<div id='locationdot_$location_id' class='dot' style='left:$location_posx"."px; top:$location_posy"."px; z-index:$zindex;'><a ";
	   if ($location_website) { $dots .= "target='_blank' href='$location_website'"; } else { $dots .= "href='javascript:;'"; }
	   $dots .= " onMouseOver=\"mousein('$location_id','$location_name_','$text',1,1,$dotsize);\" onMouseOut=\"mouseout('$location_id',$dotsize);\"><img src=\"$picsdir/$dot\" border=\"0\" id=\"dot_$location_id\"></a></div>";
	   
	   # Der Eventliste hinzufügen:
	   if ($location_website) { $location_name_linked="<a href='$location_website' target='_blank' class='list' title='$location_website'>$location_name<\/a>"; } 
	   else { $location_name_linked="<span class='list'>$location_name_</span>"; }
	   if ($event_zeit) { $zeit=zeitformat($event_zeit,$event_zeit2) ." "; }
	   else { $zeit=""; }
	   if (!$mobile) {
			$list.= "<li style='list-style-image: url($picsdir/$dot)' onMouseOver=\"mousein('$location_id','$location_name_','$text',1,0,$dotsize);\" onMouseOut=\"mouseout('$location_id',$dotsize);\">";
			if ($Row2->{audio}) {
				my $audio_text = $Row2->{audio};
				if ($audio_text =~ /^(.+)\.[^\.]{0,6}$/) { $audio_text=$1; }
				$list .= "<div class='wimpy noprint' onMouseover='mousein_audio(\"$audio_text\");' onMouseout='mouseout_audio();'>";
				$list .= wimpyCode("$audiodir/" .$Row2->{audio}, $Row2->{id}, $wimpyColor);
			} else {
				$list .= "<div class='wimpy noprint'>";
				$list .= "<img src='$picsdir/blank.gif' style='width:15px;height:15px;'>";
			}
			$list .= "</div>";
			$list .= "$location_name_linked: <i>$zeit$event_beschreibung</i></li>\n";
	   } else {
			$list.= "<li style='list-style-image: url($picsdir/$dot)'>$location_name_linked: <i>$zeit$event_beschreibung</i></li>\n";
	   }
	}

  # Location hat keinen Event
  } else {
   if (!$hasStar) {
	   if ($location_showalways || $showall == 1) {
		$text = "<b>$location_name_</b><br>$web";
		my $dotsize=7;
		$dots .= "<div id='locationdot_$location_id' class='dot' style='left:$location_posx"."px; top:$location_posy"."px;'><a ";
		if ($location_website) { $dots .= "target='_blank' href='$location_website'"; } else { $dots .= "href='javascript:;'"; }
		$dots .= " onMouseOver=\"mousein('$location_id','$location_name_','$text',1,0,$dotsize);\" onMouseOut=\"mouseout('$location_id',$dotsize);\"><img src=\"$picsdir/dot2.gif\" border=\"0\" id=\"dot_$location_id\"></a></div>";
	   }
   }
  }


   

 }
 $Select->finish();


 # Alle Events mit einer nicht eingetragenen Location in die Eventliste hinzufügen:
 $Select = $dbh->prepare("SELECT * FROM events WHERE (datum = '$datumgesucht_sql') and (enabled = '1') and (blocked = '0') and (star = '$hasStar')");
 $Select->execute;
 while ($Row=$Select->fetchrow_hashref) {
  $event_ort = $Row->{ort};
  $event_datum = $Row->{datum};
  $event_zeit = $Row->{zeit};
  $event_zeit2 = $Row->{zeit2};
  $event_beschreibung = $Row->{beschreibung};
  $event_ort_ = $event_ort;
  $event_ort_=~s/'/\\'/g;
  $event_star = $Row->{star};
  # Gibt es eine passende Location in der Datenbank?
  $Select2 = $dbh->prepare("SELECT count(*) FROM locations WHERE (name = '$event_ort_') and (enabled = '1')"); $Select2->execute; 
  # Es gibt keine passende Location:
  if ($Select2->fetchrow == 0) {
   if ($event_zeit) { $zeit=zeitformat($event_zeit,$event_zeit2) ." "; }
   else { $zeit=""; }
   my $dot="dot0.gif"; if ($event_star==1) { $dot="dot3.gif"; }

	$list.= "<li style='list-style-image: url($picsdir/$dot)'>";
	if (!$mobile) {
		if ($Row->{audio}) {
			my $audio_text = $Row->{audio};
			if ($audio_text =~ /^(.+)\.[^\.]{0,6}$/) { $audio_text=$1; }
			$list .= "<div class='wimpy noprint' onMouseover='mousein_audio(\"$audio_text\");' onMouseout='mouseout_audio();'>";
			$list .= wimpyCode("$audiodir/" .$Row->{audio}, $Row->{id}, $wimpyColor);
		} else {
			$list .= "<div class='wimpy noprint'>";
			$list .= "<img src='$picsdir/blank.gif' style='width:15px;height:15px;'>";
		}
		$list .= "</div>";
	}
	if ($event_star==1) { $list .= "<span class='list'>$event_ort</span>"; }
	else { $list .= $event_ort; }
	$list .= ": <i>$zeit";
	if (!$mobile) { $list.=add_links($event_beschreibung); } else { $list.=$event_beschreibung; }
	$list .= "</i></li>\n";
  }
  $Select2->finish;
 }
 $Select->finish();




}












sub isMobile() {
	my ($ua) = @_;
	if ($ua =~ /AvantGo/i) { return true; }
	if ($ua =~ /DoCoMo/i) { return true; }
	if ($ua =~ /KDDI/i) { return true; }
	if ($ua =~ /UP\.Browser/i) { return true; }
	if ($ua =~ /Vodafone/i) { return true; }
	if ($ua =~ /J-PHONE/i) { return true; }
	if ($ua =~ /DDIPOCKET/i) { return true; }
	if ($ua =~ /PDXGW/i) { return true; }
	if ($ua =~ /ASTEL/i) { return true; }
	if ($ua =~ /PalmOS/i) { return true; }
	if ($ua =~ /Windows CE/i) { return true; }
	if ($ua =~ /Plucker/i) { return true; }
	if ($ua =~ /NetFront/i) { return true; }
	if ($ua =~ /Xiino/i) { return true; }
	if ($ua =~ /BlackBerry/i) { return true; }
	if ($ua =~ /iPAQ/i) { return true; }
	if ($ua =~ /MIDP/i) { return true; }
	if ($ua =~ /Smartphone/i) { return true; }
	if ($ua =~ /portalmmm/i) { return true; }
	if ($ua =~ /SymbianOS/i) { return true; }
	if ($ua =~ /Series80/i) { return true; }
	if ($ua =~ /PalmSource/i) { return true; }
	if ($ua =~ /Blazer/i) { return true; }
	if ($ua =~ /Opera Mini/i) { return true; }
	if ($ua =~ /ReqwirelessWeb/i) { return true; }
	if ($ua =~ /SonyEricsson/i) { return true; }
	if ($ua =~ /PlayStation Portable/i) { return true; }
	if ($ua =~ /Sprint/i) { return true; }
	if ($ua =~ /EudoraWeb/i) { return true; }
	if ($ua =~ /Elaine/i) { return true; }

	return 0;
}






# Datum und taggesucht berechnen:
 sub getDatum() {

 ($Sekunden, $Minuten, $Stunden, $Monatstag, $Monat, $Jahr, $Wochentag) = localtime(time); $Monat+=1; $Jahr+=1900;
 my $today, $todaystamp, $versatz;
 $today = ymd2julian($Jahr,$Monat,$Monatstag);
 $todaystamp = "$Monatstag.$Monat.$Jahr";

 if ($Stunden<$morninghour) {
  ($Sekunden, $Minuten, $Stunden, $Monatstag, $Monat, $Jahr, $Wochentag) = localtime(time -86400); $Monat+=1; $Jahr+=1900;
  $today = ymd2julian($Jahr,$Monat,$Monatstag);
  $todaystamp = "$Monatstag.$Monat.$Jahr";
  $versatz=-86400;
 }

 # Looking for $taggesucht and insering date:
 $n=0;
 foreach(@tage) { 
  if ($taggesucht =~ /^$_/i) {
   my $weekoffset;
   if ($taggesucht =~ /^$_\_(\d+)$/i) { $weekoffset=$1; }
   if ($n >= $Wochentag) {
    ($tmp, $tmp, $tmp, $Monatstag2, $Monat2, $Jahr2) = localtime(time + ($n-$Wochentag +7*$weekoffset)*86400 +$versatz); $Monat2+=1; $Jahr2+=1900;
    $datumgesucht = "$Monatstag2.$Monat2.$Jahr2";
	$datumgesucht_sql = datum_sql($Jahr2, $Monat2, $Monatstag2);
    last;
   } else {
    ($tmp, $tmp, $tmp, $Monatstag2, $Monat2, $Jahr2) = localtime(time + (7-$Wochentag+$n+7*$weekoffset)*86400 +$versatz); $Monat2+=1; $Jahr2+=1900;
    $datumgesucht = "$Monatstag2.$Monat2.$Jahr2";
	$datumgesucht_sql = datum_sql($Jahr2, $Monat2, $Monatstag2);
    last;
   }
  }
  $n++;
 }

 if ($taggesucht =~ /\_(\d+)$/i) { $taggesucht="$tage[$n]_$1"; }
 else { $taggesucht=$tage[$n]; }
 
 if (!$datumgesucht) {
  $datumgesucht = "$Monatstag.$Monat.$Jahr";
  $datumgesucht_sql = datum_sql($Jahr, $Monat, $Monatstag);
  $taggesucht=$tage[$Wochentag]; 
 }
 
 
}

# Wimpy-Flash Player HTML-Code zu einer MP3-URL zurückgeben:
sub wimpyCode{
	my($audiofile, $id, $bgcolor) = @_;
	local $html='';
	local $size=15;

	#$html.= "<embed src='/wimpy_button.swf?theFile=$audiofile&playingColor=$bgcolor&theBkgdColor=$bgcolor' width='$size' height='$size' quality='high' bgcolor='#$bgcolor' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwave-flash' wmode='transparent'  name='wimpy_button_$id' />";
	$html.= "<script>writeWimpy('$audiofile', '$id', '$bgcolor', '$size')</script>";
	return $html;
}
 




# Get URLs from DB and cache them into a hash so we can insert them into events:
sub getUrls() {
	 $Select = $dbh->prepare("SELECT * FROM urls WHERE (alias is null)");
	 $Select->execute; 
	 # Loop:
	 while ($Row=$Select->fetchrow_hashref) {
		# Save in hash:
		$NameToUrl{$Row->{name}} = $Row->{url};
		$NameToOnlyifmarked{$Row->{name}} = $Row->{onlyifmarked};
	}
}




## In einem String URLs durch HTML-Links ersetzen:
sub add_links{
 my($src) = @_;

 # Replace urls:
 # Ohne Pfad:
 $src =~ s/((http:\/\/)?((www)?[\w_\.\-öäü]{3,}\.[a-z]{2,4}))([^\/\w])/<a class='url' href='http:\/\/$3' target='_blank'>$1<\/a>$5/gi;
 $src =~ s/((http:\/\/)?((www)?[\w_\.\-öäü]{3,}\.[a-z]{2,4}))$/<a class='url' href='http:\/\/$3' target='_blank'>$1<\/a>/gi;
 # Mit Pfad nach domain:
 $src =~ s/((http:\/\/)?((www)?[\w_\.\-öäü]{3,}\.[a-z]{2,4}\/[\/\w_\.\-öäü]*))([^\/\w])/<a class='url' href='http:\/\/$3' target='_blank'>$1<\/a>$5/gi;
 $src =~ s/((http:\/\/)?((www)?[\w_\.\-öäü]{3,}\.[a-z]{2,4}\/[\/\w_\.\-öäü]*))$/<a class='url' href='http:\/\/$3' target='_blank'>$1<\/a>/gi;

 # Replace urls in hash (from db):
 while ( my ($name, $url) = each(%NameToUrl) ) {
		if ($NameToOnlyifmarked{$name}) {
	        $src =~ s/\[$name\]/<a class='url' href='$url' target='_blank'>$name<\/a>/i;
		} else {
	        if (! ($src =~ s/\b$name\b/<a class='url' href='$url' target='_blank'>$name<\/a>/i)) {
				if (! ($src =~ s/\b$name([:\s])/<a class='url' href='$url' target='_blank'>$name<\/a>$1/i)) {
					$src =~ s/(\s)$name([:\s])/$1<a class='url' href='$url' target='_blank'>$name<\/a>$2/i;
				}	
			}
		}
 }

 return $src;
}



## SQL-Datum erhalten:
sub datum_sql{
 my($y, $m, $d) = @_;
 if ($d<10) { $d = "0$d"; }
 if ($m<10) { $m = "0$m"; }
 if ($y<100) { $y = "19$y"; }
 return "$y-$m-$d";
}

## 1 bis 2 SQL Zeiten formatieren:
sub zeitformat{
 my($zeit,$zeit2) = @_;
 if ($zeit2) { return zeit_sql2norm($zeit) ."-". zeit_sql2norm($zeit2); }
 else { return zeit_sql2norm($zeit); }
}

## SQL-Zeit in Form "HH:MM" umwandeln:
sub zeit_sql2norm{
 my($zeit) = @_;
 if ($zeit =~ /^0(\d:\d\d):\d\d$/) { return $1; }
 elsif ($zeit =~ /^(\d\d:\d\d):\d\d$/) { return $1; }
 else { return $zeit; }
}


 ## Gregorianisches zu julianisches Datum konvertieren:
sub ymd2julian{
 my($y, $m, $d) = @_;
  if ($m < 3) { $f = -1; } else { $f = 0; }
  return floor((1461*($f+4800+$y))/4) + floor((($m-2-($f*12))*367)/12) - floor(3*floor(($y+4900+$f)/100)/4) + $d - 32075;
}

#Nächstkleinere Ganzzahl ermitteln:
sub floor{
 $_[0] =~ /^(\d+)\.?\d*$/;
  return $1;
} 
 
 
 
 
# Escapes a string to be savely used in a SQL-query
sub escapeSql($) {
	my ($str) = @_;
	$str =~ s/\\/\\\\/g;
	$str =~ s/'/\\'/g;
	return $str;
}