#!/usr/bin/perl

use DBI;
use CGI;
#use CGI::Carp qw(fatalsToBrowser);

# HTML-Template:
my $htmlfile = "../denkmal.org_empty.html";
my $email = 'events@denkmal.org';

$query = CGI::new();
my $action = $query->param("action");
my $wo = $query->param("wo");
my $wo_name = $query->param("wo_name");
my $wo_ort = $query->param("wo_ort");
my $wo_website = $query->param("wo_website");
my $wann_datum = $query->param("wann_datum");
my $wann_zeit1 = $query->param("wann_zeit1");
my $wann_zeit2 = $query->param("wann_zeit2");
my $was = $query->param("was");
my $links = $query->param("links");
my $content, $fehlermeldung;

my $dbh = DBI->connect("DBI:mysql:withinc_denkmal:localhost","withinc_denkmal","thedog")  || die "Database error: $DBI::errstr";
$set = $Select2 = $dbh->prepare("SET NAMES utf8");
$set->execute; $set->finish;

my($Sekunden, $Minuten, $Stunden, $Monatstag, $Monat, $Jahr, $Wochentag) = localtime(time); $Monat+=1; $Jahr+=1900; $Wochentag--;


if ($action) {
	# Eingaben überprüfen:
	my $fertige_location=0;
	if ($wo ne "-") { $fertige_location=1; } else { $wo=''; }
	
	if ($fertige_location==0) {
		if ($wo_name eq "") { $fehlermeldung.="Der Name der neu eingetragenen Location ist unvollständig.<br>"; }
		if ($wo_ort eq "") { $fehlermeldung.="Der genaue Ort der neu eingetragenen Location ist unvollständig.<br>"; }
		if ($wo_website ne "" && $wo_website !~ /^http:\/\/./) { $fehlermeldung.="Die Webseite der neu eingetragenen Location ist unvollständig. Bitte im Format http://www.example.com angeben, oder das Feld leer lassen.<br>"; }
	}
	if ($wann_datum !~ /^\d{1,2}\.\d{1,2}\.\d{4}$/) { $fehlermeldung.="Die Datumsangabe ist ungültig. Bitte im Format DD.MM.YYYY angeben.<br>"; }
	if ($wann_zeit1 !~ /^\d{1,2}:\d{2}$/) { $fehlermeldung.="Die Zeitangabe ist ungültig. Bitte im Format HH:MM angeben.<br>"; }
	if ($wann_zeit2 ne "" && $wann_zeit2 !~ /^\d{1,2}:\d{2}$/) { $fehlermeldung.="Die End-Zeitangabe ist ungültig. Bitte im Format HH:MM angeben, oder das Feld leer lassen.<br>"; }
	if ($was eq "") { $fehlermeldung.="Die Beschreibung des Events ist unvollständig.<br>"; }
	$was =~ s/\r//g; $was =~ s/\n/ /g;	# Zeilenumbrüche löschen.
	
	# Sind die Eingaben vollständig?
	if ($fehlermeldung) {
		undef $action;
	} else {
		# Event eintragen:
		if ($fertige_location==0) { $wo=$wo_name; }
		$wann_datum =~ /^(\d{1,2})\.(\d{1,2})\.(\d{4})$/;
		if ($links ne '') { $was = "$was / $links"; }
		&addEvent($3,$2,$1, $wann_zeit1, $wann_zeit2, $wo, $was, '0', '1', '1');
		
		# Neue Location eintragen:
		if ($fertige_location==0) {
		
			# Apostroph für SQL-Query ersetzen:
			$wo_name =~ s/'/\\'/g;
			$wo_ort =~ s/'/\\'/g;
			$wo_website =~ s/'/\\'/g;
		
			$ins = $dbh->prepare("INSERT INTO locations (name,website,enabled) VALUES('$wo_name / $wo_ort','$wo_website','0')");
			$ins->execute;
			$ins->finish;
		}
		
		$content .= "Vielen Dank!<br><br>";
		$content .= "Ihre Eintragung wurde gespeichert und wird nun überprüft. Dies kann bis zu einem Tag dauern. Danach wird der Event, sofern er akzeptiert wurde, auf denkmal.org erscheinen.<br>";
		$content .= "<br>";
		$content .= "Sollten Sie Kommentare oder Fragen haben, so schreiben Sie doch ein E-Mail an <a href='mailto:$email'>$email</a> oder rufen Sie an oder schreiben Sie ein SMS an 079 / 819 88 77.<br>";
	}

}

if (!$action) {

	$content .= "<style>";
	$content .= ".subfield {";
	$content .= "	margin: 15px;";
	$content .= "	padding: 3px;";
	$content .= "	border: 1px solid #444444;";
	$content .= "}";
	$content .= "hr {";
	$content .= "	color: #666666; background-color: #666666;";
	$content .= "	border: 0;";
	$content .= "	height: 1px;";
	$content .= "}";
	$content .= "label {";
	$content .= "	display: block;";
	$content .= "	width: 130px;";
	$content .= "	float: left;";
	$content .= "}";
	$content .= "</style>";
	$content .= "<script>";
	$content .= "function locationChg() {";
	$content .= "	if (document.getElementById('wo').value == '-') {";
	$content .= "		document.getElementById('wo_name').disabled=false;";
	$content .= "		document.getElementById('wo_ort').disabled=false;";
	$content .= "		document.getElementById('wo_website').disabled=false;";
	$content .= "		document.getElementById('newlocation').style.display='block';";
	$content .= "		document.getElementById('info_website').innerHTML='';";
	$content .= "	} else {";
	$content .= "		document.getElementById('wo_name').disabled=true;";
	$content .= "		document.getElementById('wo_ort').disabled=true;";
	$content .= "		document.getElementById('wo_website').disabled=true;";
	$content .= "		document.getElementById('newlocation').style.display='none';";
	$content .= "		var website = document.add_event.wo.options[document.add_event.wo.selectedIndex].id;";
	$content .= "		document.getElementById('info_website').innerHTML='<a target=\"_blank\" href=\"'+website+'\">'+website+'</a>';";
	$content .= "	}";
	$content .= "}";
	$content .= "</script>";


	$content .= "Sie können mit diesem Formular einen kommenden Event in der Region Basel eintragen.<br>";
	$content .= "Jeder Eintrag wird <i>geprüft</i>, bevor er auf denkmal.org erscheint. Es kann deshalb durchaus einen Tag dauern, bis Ihr Vorschlag auf der Webseite erscheint.<br>";
	
	if ($fehlermeldung) { $content .= "<br><div style='color:red;'><b>Bitte überprüfen Sie Ihre Eingaben nochmals:</b><br>$fehlermeldung</div>"; }


	$content .= "<form method='POST' name='add_event'>";
	$content .= "<input type='hidden' name='action' value='add'>";

	$content .= "<div class='subfield'>";
	$content .= "<b>Wo:</b><br>";
	$content .= "Eine Location aus der Liste wählen:<br>";
	$content .= "<label>Location:</label> <select name='wo' id='wo' onChange='locationChg()'>";
	$content .= "<option>-</option>";
	$Select = $dbh->prepare("SELECT * FROM locations WHERE (alias is null) and (enabled = '1') ORDER by name");
	$Select->execute;
	$val="";
	while ($Row=$Select->fetchrow_hashref) {
		if ($Row->{name} eq $wo) { $content .= "<option id='$Row->{website}' SELECTED>$Row->{name}</option>"; $val=$Row->{website} }
		else { $content .= "<option id='$Row->{website}'>$Row->{name}</option>"; }
	}
	$Select->finish();
	if ($val) { $content .= "</select> <font id='info_website'><a href='$val' target='_blank'>$val</a></font><br>"; }
	else { $content .= "</select> <font id='info_website'></font><br>"; }
	$content .= "<div id='newlocation'";
	if ($wo) { $content .= " style='display:none;'"; }
	$content .= ">";
	$content .= "<hr><u>Oder</u>: Eine noch nicht gespeicherte Location angeben:<br>";
	$content .= "<label>Name:</label> <input type='text' name='wo_name' id='wo_name' value='$wo_name'> <i>(Bsp: 'Capri Bar')</i> <br>";
	$content .= "<label>Ort:</label> <input type='text' name='wo_ort' id='wo_ort' value='$wo_ort'> <i>(Bsp: 'Klybeckstrasse 240b' oder 'Beim Florabeach')</i> <br>";
	if (!$fehlermeldung) { $val="http://"; } else { $val=$wo_website; }
	$content .= "<label>Webseite:</label> <input type='text' name='wo_website' id='wo_website' value='$val'> <i>(Bsp: 'http://www.jetcoray.com' oder '')</i> <br>";
	$content .= "</div>";
	$content .= "</div>";

	$content .= "<div class='subfield'>";
	$content .= "<b>Wann:</b><br>";
	if (!$fehlermeldung) { $val="$Monatstag.$Monat.$Jahr"; } else { $val=$wann_datum; }
	$content .= "<label>Datum:</label> <input type='text' name='wann_datum' value='$val'> <i>(Bsp: '17.7.$Jahr')</i> <br>";
	if (!$fehlermeldung) { $val="22:00"; } else { $val=$wann_zeit1; }
	$content .= "<label>Zeit:</label> <input type='text' name='wann_zeit1' value='$val'> <i>(Bsp: '22:00')</i> <br>";
	if (!$fehlermeldung) { $val=""; } else { $val=$wann_zeit2; }
	$content .= "<label>Ende (optional):</label> <input type='text' name='wann_zeit2' value='$val'> <i>(Bsp: '3:00')</i> <br>";
	$content .= "</div>";

	$content .= "<div class='subfield'>";
	$content .= "<b>Was:</b><br>";
	if (!$fehlermeldung) { $val=""; } else { $val=$was; }
	$content .= "<label>Beschreibung:</label> <textarea name='was' rows='3' cols='30'>$val</textarea><br>";
	$content .= "<div style='padding-left:130px;'><i>(Bsp: 'Keiths Geburtstagsparty. Livemusik von The Examples danach DJs.')</i></div>";
	if (!$fehlermeldung) { $val=""; } else { $val=$links; }
	$content .= "<label>Webseiten:</label> <textarea name='links' rows='3' cols='30'>$val</textarea><br>";
	$content .= "<div style='padding-left:130px;'><i>(Webseiten von Bands, DJs oder Festivals zum Event.<br/>Diese Links werden gespeichert. Also nur einmal angeben!)</i></div>";
	$content .= "</div>";

	$content .= "<div class='subfield' style='text-align: right; border:0;'>";
	$content .= "<input type='submit' value='Abschicken'>";
	$content .= "</div>";

	$content .= "</form>";

}




$dbh->disconnect();

# Das ganze im Template ausgeben:
open(HTML,$htmlfile);@html=<HTML>;
close(HTML);
$i=0;
foreach(@html) {
 $html[$i]=~s/\[TITLE\]/Einen Event hinzufügen/;
 $html[$i]=~s/\[CONTENT\]/$content/g;
 $i++;
}
print "Content-type: text/html\n\n";
print @html;









## Einen Event in der Datebank eintragen
sub addEvent() {
	my($y,$m,$d, $zeit1, $zeit2, $wo, $was, $enabled, $nodeletion, $star) = @_;
	
	# Apostroph für SQL-Query ersetzen:
	$was =~ s/'/\\'/g;
	$wo =~ s/'/\\'/g;
	
	# Überprüfen, ob ein Alias für die Location besteht:
	$Select2 = $dbh->prepare("SELECT * FROM locations WHERE (name = '$wo') and (alias is not null) and (enabled = '1')");
	$Select2->execute; 
	$Row2=$Select2->fetchrow_hashref;
	if ($Row2) { $wo = $Row2->{alias}; }
	$Select2->finish();
	
	
	# Eintrag hinzufügen:
	$zeit1 = zeit_sql($zeit1);
	$zeit2 = zeit_sql($zeit2);
	if ($zeit2) {
		$ins = $dbh->prepare("INSERT INTO events (datum, zeit, zeit2, ort, beschreibung, enabled, nodeletion, star) VALUES('$y-$m-$d','$zeit1','$zeit2','$wo','$was','$enabled','$nodeletion','$star')");
	} else {
		$ins = $dbh->prepare("INSERT INTO events (datum, zeit, ort, beschreibung, enabled, nodeletion, star) VALUES('$y-$m-$d','$zeit1','$wo','$was','$enabled','$nodeletion','$star')");
	}
	$ins->execute;
	$ins->finish;
}








## Zeit formatieren:
sub zeit() {
 $zeit = $_[0];
 $zeit =~ s/(\d?\d)[\.:](\d\d)/$1:$2/g;
 return $zeit;
}

## Zeit nach SQL formatieren:
sub zeit_sql() {
 $zeit = $_[0];
 $zeit =~ s/(\d?\d)[\.:](\d\d)/$1:$2:00/g;
 return $zeit;
}



## Gregorianisches zu julianisches Datum konvertieren:
sub ymd2julian{
 my($y, $m, $d) = @_;
  if ($m < 3) { $f = -1; } else { $f = 0; }
  return floor((1461*($f+4800+$y))/4) + floor((($m-2-($f*12))*367)/12) - floor(3*floor(($y+4900+$f)/100)/4) + $d - 32075;
}

#Nächstkleinere Ganzzahl ermitteln:
sub floor{
 $_[0] =~ /^(\d+)\.?\d*$/;
  return $1;
}





	