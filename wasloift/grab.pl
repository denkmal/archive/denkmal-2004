#!/usr/bin/perl

use DBI;
use LWP::UserAgent;
use CGI::Carp qw(fatalsToBrowser);
use Encode;

# Number of days to update from today:
$updatedays = 9;

# Aktuelles Datum:
my ($now_Sekunden, $now_Minuten, $now_Stunden, $now_Monatstag, $now_Monat, $now_Jahr, $now_Wochentag) = localtime(time); $Monat+=1; $Jahr+=1900;

my $ua = LWP::UserAgent->new;
$ua->agent('Mozilla/5.0');
$|=1;
my $dbh = DBI->connect("DBI:mysql:withinc_denkmal:localhost","withinc_denkmal","thedog")  || die "Database error: $DBI::errstr";
$set = $Select2 = $dbh->prepare("SET NAMES utf8");
$set->execute; $set->finish;



print "Content-Type: text/html\n\n";
print '<html><head><meta http-equiv="Content-Type" content="text/html;charset=utf-8"></head><body>';



# Get Events from Eventcalendars by day:
for ($i=0; $i<$updatedays; $i++) {
	($tmp, $tmp, $tmp, $Monatstag, $Monat, $Jahr) = localtime(time + $i*86400); $Monat+=1; $Jahr+=1900;
	print "Updating $Monatstag.$Monat.$Jahr... ";

	&update20min($Jahr,$Monat,$Monatstag, "konzerte");
	&update20min($Jahr,$Monat,$Monatstag, "party");
	&updateProgrammzeitung($Jahr,$Monat,$Monatstag);
	print "<br>\n";
}

# Get events from location-sites etc:
print "Updating Location-Events:";
&updateLocPage("Chez Soif");
&updateLocPage("Gleis 13");
&updateLocPage("1. Stock Schoolyard");
&updateLocPageViaMyspace("Funambolo", "226345070");

print "<br>";


# Delete current Meteo-pics from drs.ch:
system('rm ../pics/meteo/*');
# Get Meteo-pics from drs.ch:
print "Updating Meteo-pics... ";
for ($i=1; $i<=4; $i++) {
	saveMeteo("http://www.drs.ch/images/meteo/meteo0$i.gif");
	print "$i ";
}
print "<br>\n";

# Drop cache:
print "Dropping cache...";
dropCache();


$dbh->disconnect();


print '</body></html>';





######################################################################################

# Save a url to the meteo-pics folder
sub saveMeteo($) {
	my($url) = @_;
	system("wget -P '../pics/meteo/' '$url'");
}



## Einen Event in der Datebank eintragen
sub addEvent($$$$$$$$$$) {
	my($y,$m,$d, $zeit1, $zeit2, $wo, $was, $enabled, $nodeletion, $star) = @_;

	# Überprüfen, ob wir nicht einen Event in der Vergangenheit eintragen, das wollen wir nicht:
	if ($y<$now_Jahr) { return; }
	if ($y==$now_Jahr && $m<$now_Monat) { return; }
	if ($y==$now_Jahr && $m==$now_Monat && $d<now_$Monatstag) { return; }
	
	# Apostrophe ersetzen:
	$was =~ s/["`]/'/g;	#'"
	$was = escapeSql($was);
	$wo = escapeSql($wo);

	# "Caps-Lock" Wörter Kleinschreiben (3 oder mehr Zeichen):
	$was =~ s/\b([A-ZÖÄÜ])([A-ZÖÄÜ]{2,})\b/$1\L$2\E/g;

	# Unnötige Leerzeichen entfernen:
	$was =~ s/(\s+)$//g;
	$was =~ s/^(\s+)//g;

	
	# Überprüfen, ob ein Alias für die Location besteht:
	$wo = doAlias($wo);
	if ($wo eq "block") { return; }

	# Wenn es an diesem tag in dieser Location schon einen protected-Eintrag gibt, den Event _nicht_ einfügen
	$count = $dbh->prepare("SELECT count(*) FROM events WHERE (datum = '$y-$m-$d') and (ort = '$wo') and (nodeletion = '1')"); $count->execute;
	$num=$count->fetchrow; $count->finish();

	
	if ($num == 0) {
		# Schauen, ob genau dieser Eintrag schon existiert:
		my $count = $dbh->prepare("SELECT count(*) FROM events WHERE (datum = '$y-$m-$d') and (ort = '$wo') and (beschreibung = '$was') and (zeit = '$zeit1')"); $count->execute;
		my $num=$count->fetchrow; $count->finish();

		# Wenn genau dieser Eintrag noch nicht existiert, dann eintragen:
		if ($num==0) {
			# Schon existierende Einträge an diesem Tag in dieser Location in der DB löschen:
			$del = $dbh->prepare("DELETE FROM events WHERE ((datum = '$y-$m-$d') and (ort = '$wo') and (nodeletion = '0'))");
			$del->execute;
			$del->finish;

			
			# Eintrag hinzufügen:
			$zeit1 = zeit_sql($zeit1);
			$zeit2 = zeit_sql($zeit2);
			if ($zeit2) {
				$ins = $dbh->prepare("INSERT INTO events (datum, zeit, zeit2, ort, beschreibung, enabled, nodeletion, star) VALUES('$y-$m-$d','$zeit1','$zeit2','$wo','$was','$enabled','$nodeletion','$star')");
			} else {
				$ins = $dbh->prepare("INSERT INTO events (datum, zeit, ort, beschreibung, enabled, nodeletion, star) VALUES('$y-$m-$d','$zeit1','$wo','$was','$enabled','$nodeletion','$star')");
			}
			$ins->execute;
			$ins->finish;

		# Genau dieser Eintrag existiert schon:
		} else {

		}
	}

	return 1;
}




# Get events from a location-page:
sub updateLocPage() {
	my($locname) = @_;
	my $programmurl;
	if ($locname eq 'Chez Soif') { $programmurl = "http://www.chezsoif.ch/unsereveranstaltungen.html"; }
	elsif ($locname eq 'Gleis 13') { $programmurl = "http://www.gleis13.com/index.php?id=4"; }
	elsif ($locname eq '1. Stock Schoolyard') { $programmurl = "http://www.schoolyard.ch/home.php?pgnum=11&section=1"; }
	 print " $locname";
	 my $numevts=0;
	 
	   $req = new HTTP::Request (GET => $programmurl);
	   $req->content_type("text/xml; charset=utf-8");
	   my $response = $ua->request($req);
	   if ($response->is_success) {
			my $src=$response->content;
			# Von latin1 nach UTF-8 konvertieren:
			Encode::from_to($src, "iso-8859-1", "utf8");
			$src=~s/[\n\r\t]/ /g;
			$src=decode_entities($src);
			# Events suchen:
			if ($locname eq 'Chez Soif') {
				while ($src =~ s#(\d{1,2})\.(\d{1,2})\.(\d{2,4}) ([^<]+)##) {
					my $d=$1; my $m=$2; my $y=$3;
					my $was = $4;
					my $wann = '21:00';
					if ($y=~/^\d{2}$/) { $y='20'.$y; }
					# Den Eintrag der DB hinzufügen:
					if (addEvent($y,$m,$d, $wann, '', $locname, $was, '0', '0', '1') == 1) {
						$numevts++;
					}
				}
			} elsif ($locname eq 'Gleis 13') {
				if ($src =~ m#<!--  Text: \[begin\] -->(.+?)<!--  Text: \[end\] -->#) {
					$src = $1;
					my @eventsrcs = split('<p class="bodytext"> </p>', $src);
					foreach $eventsrc (@eventsrcs) {
						$eventsrc =~ s/<\/p><p class="bodytext">/ /g;
						$eventsrc =~ s/<[^>]+>//g;
						if ($eventsrc =~ s#(\d{1,2})\.(\d{1,2})\.(\d{2,4}) ([^<]+)##) {
							my $d=$1; my $m=$2; my $y=$3;
							my $was = $4;
							my $wann = '21:00';
							if ($y=~/^\d{2}$/) { $y='20'.$y; }
							# Den Eintrag der DB hinzufügen:
							if (addEvent($y,$m,$d, $wann, '', $locname, $was, '0', '0', '1') == 1) {
								$numevts++;
							}
						} elsif ($eventsrc =~ s#(\d{1,2})\.(\d{1,2})\. ([^<]+)##) {
							my $d=$1; my $m=$2; my $y='2007';
							my $was = $3;
							my $wann = '21:00';
							# Den Eintrag der DB hinzufügen:
							if (addEvent($y,$m,$d, $wann, '', $locname, $was, '0', '0', '1') == 1) {
								$numevts++;
							}
						}
					}
				}
			} elsif ($locname eq '1. Stock Schoolyard') {
				if ($src =~ m#src = 'slashes2.gif'/>(.+?)<div id = "navigation">#) {
					$src = $1;
					my @eventsrcs = split("<span class = 'strong'>", $src);
					foreach $eventsrc (@eventsrcs) {
						$eventsrc =~ s/<\/p>\s*<p>/ /g;
						$eventsrc =~ s/<[^>]+>//g;
						$eventsrc =~ s/\s+/ /g;
						if ($eventsrc =~ s#(\d{1,2})\.(\d{1,2})\.(\d{2,4}), türe (\d{1,2}):(\d{2}) (.+)##) {
							my $d=$1; my $m=$2; my $y=$3;
							my $wann = "$4:$5";
							my $was = $6;
							if ($y=~/^\d{2}$/) { $y='20'.$y; }
							$was =~ s/"//g;	#"
							$was =~ s/ \| /: /g;
							if ($was !~ /^tba\b/) {
								# Den Eintrag der DB hinzufügen:
								if (addEvent($y,$m,$d, $wann, '', $locname, $was, '0', '0', '1') == 1) {
									$numevts++;
								}
							}
						}
					}
				}
			}
	   }
	 print "[$numevts] ";

}


# Get events from a myspace-location-page:
sub updateLocPageViaMyspace($$) {
	my($locname, $myspaceid) = @_;

	my $programmurl = "http://collect.myspace.com/index.cfm?fuseaction=bandprofile.listAllShows&friendid=$myspaceid";
	print " $locname";
	my $numevts=0;
	 
	   $req = new HTTP::Request (GET => $programmurl);
	   #$req->content_type("text/xml; charset=utf-8");
	   my $response = $ua->request($req);
	   if ($response->is_success) {
			my $src=$response->content;
			# Von latin1 nach UTF-8 konvertieren:
			#Encode::from_to($src, "iso-8859-1", "utf8");
			$src=~s/[\n\r\t]/ /g;
			$src=decode_entities($src);
			# Events suchen:
			while ($src =~ s#<td><b>(\d{2})/(\d{2})/(\d{4}) (\d{2}):(\d{2}) (\w{2})</b> - <b>([^<]+?)</b></td>\s*<td rowspan="2" align="right"><input.*?</td>\s*</tr>\s*<tr valign="top">\s*<td>([^<]*)<br /><br />([^<]*)</td>##) {
				my $d=$2; my $m=$1; my $y=$3;
				my $wann;
				if ($6 eq 'PM') { $wann = ($4+12). ":$5"; }
				my $was = $7;
				my $was2 = $9;
				$was =~ s/at $locname basel//ig;
				$was =~ s/at $locname//ig;
				$was =~ s/^\s+//; $was =~ s/\s+$//;
				if ($was !~ /[\.!\?]$/ && $was2 ne '') { $was .= "."; }
				if ($was2 ne '') { $was .= " $was2"; }

				if ($was =~ /\btba\b/i) {
				} else {
					#Den Eintrag der DB hinzufügen:
					if (addEvent($y,$m,$d, $wann, '', $locname, $was, '0', '0', '1') == 1) {
						$numevts++;
					}
				}
			}

		}
	 print "[$numevts] ";


}



## Bringt die Event-Datenbank vom angegebenen Datum auf den neusten Stand
## 20min.ch
sub update20min() {
 my($y, $m, $d, $kategorie) = @_;
 my $datumgesucht = ymd220min($y,$m,$d);
 my $url = "http://www.20min.ch/whats_up/$kategorie/?dates=$datumgesucht+00:00:00|$datumgesucht+23:59:59&region=BS&genre=all";
 print "20min($kategorie)";
 my $numevts=0;
 
   $req = new HTTP::Request (GET => $url);
   $req->content_type("text/xml; charset=utf-8");
   my $response = $ua->request($req);
   if ($response->is_success) {
    $src=$response->content;
	# Von latin1 nach UTF-8 konvertieren:
	Encode::from_to($src, "iso-8859-1", "utf8");
    $src=~s/[\n\r\t]//g;
	$src=decode_entities($src);
	while ($src =~ s/<tr>\s*<td[^>]*>\w+,<br>.+? \/ (.+?)<\/td>\s*<td[^>]*><a[^>]*>(.+?)<\/a><\/td>\s*<td[^>]*><a[^>]*>(.+?)<\/a><\/td>\s*<\/tr>//) {
		$wann = $1; $was = $2; $wo = $3;
		if ($wo =~ /^(.+),[^,]*$/) { $wo = $1; }
		$wo =~ s/<.+?>//g;

		# Den Eintrag der DB hinzufügen:
		&addEvent($y,$m,$d, $wann, '', $wo, $was, '1', '0', '0');
		$numevts++;

	}
   }
 print "[$numevts] ";
}




## Bringt die Event-Datenbank vom angegebenen Datum auf den neusten Stand
## programmzeitung.ch
sub updateProgrammzeitung() {
 my($y, $m, $d) = @_;
 my $datumgesucht = ymd2pz($y,$m,$d);
 my $url = "http://www.programmzeitung.ch/index.cfm?Datum_von=$datumgesucht&Datum_bis=$datumgesucht&Rubrik=6&uuid=2BCD9733D9D9424C4EF093B3E35CB44B";
 print "Programmzeitung";
 my $numevts=0;
 
   $req = new HTTP::Request (GET => $url);
   $req->content_type("text/xml; charset=utf-8");
   my $response = $ua->request($req);
   if ($response->is_success) {
    $src=$response->content;
    $src=~s/[\n\r\t]//g;
	$src=decode_entities($src);
	while ($src =~ s/<div class="contentrahmen"><div class="veranstaltung">(.+?)<\/div><div class="ort">(.+?)<\/div><div class="zeit">(.+?)<\/div><\/div>//) {
		$was = $1; $wo = $2; $wann = $3;
		if ($was !~ /^Keine Veranstaltung in dieser Rubrik zum heutigen Datum gefunden./i) {
			$wann1=$wann2='';
			if ($was =~ /^<b>(.+?)<\/b>\s*([^\s].+)$/) { $was = "$1: $2"; }
			elsif ($was =~ /^<b>(.+?)<\/b>\s*$/) { $was = "$1"; }
			$was =~ s/<.+?>//g;
			if ($wo =~ /^(.+),[^,]*$/) { $wo = $1; }
			$wo =~ s/<.+?>//g;
			$wo =~ s/ \[.+\]$//g;
			if ($wann =~ /^(\d{1,2}[\.:]\d{2}) - (\d{1,2}[\.:]\d{2})$/) { $wann1=zeit($1); $wann2=zeit($2); }
			elsif ($wann =~ /^(\d{1,2}[\.:]\d{2}) \|/) { $wann1=zeit($1); }
			elsif ($wann =~ /^(\d{1,2}[\.:]\d{2})$/) { $wann1=zeit($1); }
			elsif ($wann =~ /^(\d{1,2}[\.:]\d{1})$/) { $wann1=zeit($1. "0"); }
			elsif ($wann =~ /^(\d{1,2})$/) { $wann1=zeit($1 . "d:00"); }

			# Den Eintrag der DB hinzufügen:
			&addEvent($y,$m,$d, $wann1, $wann2, $wo, $was, '1', '0', '0');
			$numevts++;
		}

	}
   }
 print "[$numevts] ";
}



# Überprüfen, ob ein Alias für die Location besteht:
sub doAlias() {
	local($loc) = @_;
	local $maybealias = 1;
	local $i=0;

	while ($maybealias == 1) {
		$Select2 = $dbh->prepare("SELECT * FROM locations WHERE (name = '$loc') and (alias is not null) and (enabled = '1')");
		$Select2->execute; 
		$Row2=$Select2->fetchrow_hashref;
		if ($Row2) { 
			if ($Row2->{alias} eq "block") { return "block"; }
			$loc = $Row2->{alias};
		} else {
			$maybealias = 0;
		}
		$Select2->finish();
		$i++;
		if ($i>5) { $maybealias = 0; }
	}
	return $loc;
}



## Gibt einem String UTF8-Flag:
sub decode_entities() {
	$str = $_[0];
	$str=~s/&nbsp;/ /g;
	$str=~s/&ndash;/-/g;
	$str=~s/&amp;/&/g;
	$str=~s/&auml;/ä/g;
	$str=~s/&eacute;/é/g;
	$str=~s/&egrave;/è/g;
	$str=~s/&ntilde;/ñ/g;
	$str=~s/&ouml;/ö/g;
	$str=~s/&uuml;/ü/g;
	$str=~s/&quot;/"/g;	#"
	return $str;
}



## Zeit formatieren:
sub zeit() {
 $zeit = $_[0];
 $zeit =~ s/(\d?\d)[\.:](\d\d)/$1:$2/g;
 return $zeit;
}

## Zeit nach SQL formatieren:
sub zeit_sql() {
 $zeit = $_[0];
 $zeit =~ s/(\d?\d)[\.:](\d\d)/$1:$2:00/g;
 return $zeit;
}

## Datum nach Programmzeitung formatieren:
sub ymd2pz{
 my($y, $m, $d) = @_;
  if ($d =~ /^\d$/) { $d="0$d"; }
  if ($m =~ /^\d$/) { $m="0$m"; }
  if ($y =~ /^\d\d$/) { $y="19$y"; }
  return "$d.$m.$y";
}
## Datum nach 20min formatieren:
sub ymd220min{
 my($y, $m, $d) = @_;
  if ($d =~ /^\d$/) { $d="0$d"; }
  if ($m =~ /^\d$/) { $m="0$m"; }
  if ($y =~ /^\d\d$/) { $y="19$y"; }
  return "$y-$m-$d";
}



## Gregorianisches zu julianisches Datum konvertieren:
sub ymd2julian{
 my($y, $m, $d) = @_;
  if ($m < 3) { $f = -1; } else { $f = 0; }
  return floor((1461*($f+4800+$y))/4) + floor((($m-2-($f*12))*367)/12) - floor(3*floor(($y+4900+$f)/100)/4) + $d - 32075;
}

#Nächstkleinere Ganzzahl ermitteln:
sub floor{
 $_[0] =~ /^(\d+)\.?\d*$/;
  return $1;
}


# Escapes a string to be savely used in a SQL-query
sub escapeSql($) {
	my ($str) = @_;
	$str =~ s/\\/\\\\/g;
	$str =~ s/'/\\'/g;
	return $str;
}



	

# Deletes all cache-rows in DB, so everything is new calculated:
sub dropCache() {
	my $del = $dbh->prepare("DELETE FROM cache WHERE (1)");
	$del->execute;
	$del->finish;
}