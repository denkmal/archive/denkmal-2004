var visible_location=0;
var visible_audio_info=0;
var posx;
var posy;


var IE;

function mousein(id, loc, text, box, fixbox, dotsize) {
	if (document.getElementById('dot_' + id).width>0) {
		document.getElementById('info').innerHTML=text;
		document.getElementById('dot_' + id).width = dotsize+2;
		if (box==1) {
			if (fixbox==1) {
				var locationdot = document.getElementById('locationdot_' + id);
				moveLocation(getX(locationdot), getY(locationdot));
			} else {
				//moveLocation(posx, posy);
				var locationdot = document.getElementById('locationdot_' + id);
				moveLocation(getX(locationdot), getY(locationdot));
			}
			document.getElementById('location').innerHTML='&nbsp;' + loc + '&nbsp;';
			document.getElementById('location').style.visibility='visible';
			if (fixbox!=1) {
				//visible_location=1;
			}
		}
	}
}

function mousein_audio(text) {
	moveAudio(posx, posy);
	document.getElementById('audio_info').innerHTML='&nbsp;' + text + '&nbsp;';
	document.getElementById('audio_info').style.visibility='visible';
	visible_audio_info=1;
}



function mouseout(id, dotsize) {
	if (document.getElementById('dot_' + id).width>0) {
		document.getElementById('location').style.visibility='hidden';
		document.getElementById('dot_' + id).width = dotsize;
		visible_location=0;
	}
}

function mouseout_audio() {
	document.getElementById('audio_info').style.visibility='hidden';
	visible_audio_info=0;
}



function mousemove(e) {
	// Get mouse-pos:
	if (IE) { // grab the x-y pos.s if browser is IE
		posx = event.clientX + document.body.scrollLeft;
		posy = event.clientY + document.body.scrollTop;
	} else {  // grab the x-y pos.s if browser is NS
		posx = e.pageX;
		posy = e.pageY;
	}  
	if (posx < 0){posx = 0;}
	if (posy < 0){posy = 0;}  

	if (visible_location==1) { moveLocation(posx, posy); }
	if (visible_audio_info==1) { moveAudio(posx, posy); }
}

function moveLocation(x,y) {
	document.getElementById('location').style.left=x+12;
	document.getElementById('location').style.top=y;
}
function moveAudio(x,y) {
	document.getElementById('audio_info').style.left=x+10;
	document.getElementById('audio_info').style.top=y+10;
}










// Get position of an element:
function getX(el) {
	var offset = 0;
	do { offset += el['offsetLeft'] || 0; el = el.offsetParent; } while(el);
	return offset;
}
function getY(el) {
	var offset = 0;
	do { offset += el['offsetTop'] || 0; el = el.offsetParent; } while(el);
	return offset;
}






// Writes an embed-tag to show a flash-wimpy player which can play the given mp3-file.
function writeWimpy(audiofile, id, bgcolor, size) {
	document.write('<embed src="/wimpy_button.swf?theFile=' +audiofile+ '&playingColor=' +bgcolor+ '&theBkgdColor=' +bgcolor+ '" width="' +size+ '" height="' +size+ '" quality="high" bgcolor="' +bgcolor+ '" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"  name="wimpy_button_' +id+ '"/>');

}
